# Hello!
My name is __Gaeel&nbsp;Bradshaw‑Rodriguez__    
  
I am a designer and programmer, primarily interested in interactivity, digital art, and computer-enabled experiences   


# Things
Some things I'm particularly proud of being involved with

## void.garden
![void.garden logo](voidgarden.700w.jpg)
void.garden is a project/collective that aims to develop art, events, media and stuff, all tangential to video games.

The event we're proudest of is [L'indécadence](https://voidgarden.itch.io/lindecadence). This was an indie games & art party taking place during IndieCade Europe in Paris. Nine games and four musicians were on the lineup. We featured colourful and unconventional projects from around the world. You can relive the event through [Anna-Céléstine's](https://twitter.com/lafilledct) photo gallery [on Facebook](https://www.facebook.com/pg/lafilledct/photos/?tab=album&album_id=1471957822887747).

As part of void.garden, I'm running a social network based on [Darius Kazemi](http://tinysubversions.com/)'s article "[runyourown.social](https://runyourown.social)". It's a micro-scale community, serving as a home to a small handful of creative people. Because it's built upon [Mastodon](https://joinmastodon.org/), this small network is connected to and can interact with the wider [fediverse](https://en.wikipedia.org/wiki/Fediverse)   


## .lazr.
![".lazr." text on a dark CRT screen](lazr.700w.jpg)
.lazr. is a local multiplayer arena shooter with deliciously glitchy visuals. Up to four players using standard XBox controllers face off in a number of game modes.

I made this game while living in a friend's spare room after dropping out of college. There were often people at the house, and we'd play a lot of local multiplayer games, like Samurai Gunn and Towerfall. I started making this game in a weird attempt to try and mix this newfound love of local competitive play with some of the experiences I had online playing Warsow and Quake 2.   
Because of this context, .lazr. became a game that can be weirdly intense and cerebral when played by people who already know how to play, but also very weird and opaque for people who don't.
 
If you'd like to learn to play .lazr., I recommend finding a friend who is about as competitive as you and spending an evening figuring out the movement mechanics. (Pro-tip, all of the weapons affect your movement in some way, and you're "slippery" as long as the trigger is held down)   
[Download here](https://gaeel.itch.io/lazr)


## The Paper Sail
![A paper boat on a screen](papersail.700w.jpg)
A collaboration with [Cosmografik](http://cosmografik.fr/), The Paper Sail is a meditative experience where the user places a small paper sail on their touch-capable device (phone, tablet, Cintiq, etc…), and then explores a quiet archipelago. They'll meet insects, fish, and occasionally, another traveller.   
This project went through quite a few design iterations, and ended up being a very small and peaceful space

I'm proud of the fauna and flora. The dragonflies excitedly flutter around, landing on the lilies, and I spent quite some time bringing the big koi fish to life.

This project also served as an experiment for a concept I'd like to explore more, which is indirect multiplayer play. If other people play The Paper Sail at the same time as you, there's a chance you'll meet them. I found that when I'd meet other boats, playful moments would happen. We'd try to sail side by side, or dance in lagoons, circling around each other.   
These fleeting, non-verbal interactions with strangers is something I've been interested in for a while. I first toyed with this idea during a Ludum Dare with [a game about connected gardens](https://twitter.com/_Gaeel_/status/1105939396442972160), and I'm trying to find a way to make it work with my [lost.spaceshipsin.space](https://lost.spaceshipsin.space) project

You can try The Paper Sail [here](https://papersail.lab.arte.tv/), I recommend playing on a large tablet device in a dark room with a tiny paper sail cut out of cigarette paper for the best effect

# Get in touch <a name="contact" aria-hidden="true"></a>
E-Mail
  * [gaeel@spaceshipsin.space](mailto:gaeel@spaceshipsin.space)
  
Social:
  * <a rel="me" href="https://pings.spaceshipsin.space">@gaeel@pings.spaceshipsin.space</a>
  * <a rel="me" href="https://twitter.com/_gaeel_">@\_gaeel\_</a>

Other identities:
  * [Glitch](https://glitch.com/@gaeel)
  * [Itch.io](https://gaeel.itch.io/)
  * [GitHub](https://github.com/Bradshaw)
  * [SoundCloud](https://soundcloud.com/freelancer-epic)
  * [YouTube](https://www.youtube.com/channel/UCHY1DlIho8kNykWSpDHL7qQ)
  * [Keybase](https://keybase.io/gaeel)
  
%YAML 1.2
---
status: unpublished
  